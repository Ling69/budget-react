# Projet Gestion de Budget

## L'objectif de ce projet est de créer une application de gestion de budget avec un back en Node/Express et un front en React.

### Server (Back-end)

L'objectif de cette partie va être de concevoir et mettre en place la base de données pour une application de gestion du budget du coté server.

## Description du projet backend

### Création de la base de données

### I/Conception

![budget-react](https://gitlab.com/Ling69/budget-react/-/raw/master/diagramme/%20budget.png)

> Diagramme des cas d'utilisation (use-case diagram) : représentation des possibilités d'interaction entre le système et les acteurs (intervenants extérieurs au système), c'est-à-dire de toutes les fonctionnalités que doit fournir le système.

> Création du Use case en diagramme UML avec starUML, qui me permet de lister toutes les fonctionnalités qui seront possibles sur mon budget.

> L'application doit permettre de gérer mon budget : Indiquer les différentes dépenses (et éventuellement les entrées) d'argent avec le montant, la catégorie de dépense/entrée et un petit titre si on souhaite le préciser.

### II/Controller

> Verifie si toutes les fonctions fonctionnent correctement par le Talend API Testet

### III/Entity, Repository

> Un dossier Entity:

* avec à l'intérieur un fichier class par table qui contiendra, dans le constructor, les propriétés.

> Ensuite un dossier Repository pour toutes méthodes :

* findAll()
* search(search)
* findById(id)
* findByDate(month)
* add()
* update()
* delete()

> Script SQL

* des script(s) SQL de table.
* un projet node.js avec mysql2 puis coder un repository/DAO pour  le table.

**entity**

* une application express et quelques routes pour les repository.


## Description du projet Front-end

### Client (Front-end)

L'objectif de cette partie utilise RESET API et la librairie React pour afficher cette application de gestion du budget (UI) du coté client.

#### commentaire

Si je n'ai pas bien mentionné tout ce qu'on doit indiquer dans notre projet, merci de me le préciser. Merci d'avance.

**le lien vers déploiement Heroku**: 
[heroku](https://ling-projet-budget.herokuapp.com)

**le lien vers depôt GITLAB**: 
[projet-budget](https://gitlab.com/Ling69/projet-budget)

[budget-react](https://gitlab.com/Ling69/budget-react)
