
export function Month({selectMonth, reset}) {

    function handleSelect(month){
        selectMonth(month)
    }

    function handleReset(){
        reset()
    }

    function capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    function getMonthsForLocale(locale) {
        let format = new Intl.DateTimeFormat(locale, { month: 'long' })
        let months = []
        for (let month = 0; month < 12; month++) {
            let testDate = new Date(Date.UTC(2000, month, 1, 0, 0, 0));
            months.push(capitalizeFirstLetter(format.format(testDate)))
        }
        return months;
    }

    const months = getMonthsForLocale('en-EN') 

    return (
        <div className="dropdown">
            <button className="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Choice Month
            </button>
            <div className="dropdown-menu" style={{lineHeight:"1em"}} aria-labelledby="dropdownMenuButton">
                <p onClick={handleReset} className="dropdown-item">Months</p>
                {months.map((month,index)=> <p key={index} onClick={()=>handleSelect(index+1)} className="dropdown-item">{month}</p> )}
            </div>
        </div>
    )
}
