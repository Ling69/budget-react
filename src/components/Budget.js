
import { Link } from 'react-router-dom';
import './Budget.css'


export function Budget({ budget, onDelete }) {

    return (
       
    <tr>
      <th scope="row">{budget.id}</th>
      <td>{budget.category}</td>
      <td>{budget.title}</td>
      <td>{budget.price} €</td>
      <td className="test">{budget.date}  
      <Link className="btn btn-secondary" to={"/more/"+budget.id}>More</Link>
      <button className="btn btn-danger" onClick={() => onDelete(budget.id)}>X</button></td>
    </tr>
          
    );
}
