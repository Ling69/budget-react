import { useState } from "react";
import './BudgetForm.css'

const initialState = {
    category: "",
    title: "",
    price: "",
    date: ""
}

export function BudgetForm ({onFormSubmit, budget = initialState, close}) {

    const [form, setForm] = useState(budget);

    function handleChange(event) {
        setForm({
            ...form,
            [event.target.name]: event.target.value
        });
    }

    function handleSubmit(event) {
        event.preventDefault();
        onFormSubmit(form);   
        close()  
    }

    return (
        <form onSubmit={ handleSubmit }>
            <label htmlFor="category">category</label>
            <input className="form-control" id="category" required type="text" name="category" onChange={handleChange} />
            <label htmlFor="title">title</label>
            <input className="form-control" id="title" required type="text" name="title" onChange={handleChange} />
            <label htmlFor="price">price</label>
            <input className="form-control" id="price" required type="price" name="price" onChange={handleChange}  />
            <label htmlFor="date">date</label>
            <input className="form-control" id="date" required type="date" name="date" onChange={handleChange}  />
            <button className="btn btn-primary">Save</button> 
        </form>
    )
    }
