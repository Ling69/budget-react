
import './App.css';
import React from 'react';
import { BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import { Home } from './pages/Home';
import { More } from './pages/More';
import { AddExpenses } from './pages/AddExpenses';


function App() {
  return (
    <Router>
    <div className="container-fluid">
               
      <Switch>
        <Route path="/" exact>
      <Home />
        </Route>
        <Route path="/more/:id">
          <More />
        </Route>
       </Switch>
    </div>
    </Router>
  );
}


export default App;