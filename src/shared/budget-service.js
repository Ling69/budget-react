import axios from "axios";

export class BudgetService {

    static async fetchAll() {
        return axios.get(process.env.REACT_APP_SERVER_URL+'/api/budget');
        
    }

    static async delete(id) {
        return axios.delete(process.env.REACT_APP_SERVER_URL+'/api/budget/' + id);
    }

    static async add(budget) {
        return axios.post(process.env.REACT_APP_SERVER_URL+'/api/budget', budget);
    }

    static async fetchOne(id) {
        return axios.get(process.env.REACT_APP_SERVER_URL+'/api/budget/' + id);
    }

    static async search(search) {

        const response = await axios.get(process.env.REACT_APP_SERVER_URL+'/api/budget?search='+search);
        return response.data;
    }


    static async findByDate(month) {

        const response = await axios.get(process.env.REACT_APP_SERVER_URL+ '/api/budget/month/'+ month);
        return response.data;

    }

}

