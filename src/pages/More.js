import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { BudgetService } from "../shared/budget-service";


export function More() {

    const [budget, setBudget] = useState(null);
    const { id } = useParams();

    //useEffect(()=>{},[id]);
    useEffect(() => {

        async function fetch() {
            // BudgetService.fetchOne(id)
            //    .then(response => setBudget(response.data));

            const response = await BudgetService.fetchOne(id);
            setBudget(response.data);
        }
        fetch();
    }, [id]);

    if (budget) {
        return (
            <div>
                <p>Id: {budget.id}</p>
                <p>Category: {budget.category}</p>
                <p>Title: {budget.title} </p>
                <p>Price: {budget.price} </p>
                <p>Date: {budget.date} </p>
            </div>
        )
    } else {
        return (
            <div>
                loading...
            </div>
        )
    }
}