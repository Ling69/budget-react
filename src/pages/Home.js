import './Home.css';
import { useEffect, useState } from "react";
import { Budget } from "../components/Budget";
import { BudgetService } from "../shared/budget-service";
import { AddExpenses } from "./AddExpenses";
import { SearchBar } from '../components/SearchBar';
import { Month } from '../components/Month';

export function Home() {

    const [budgets, setBudgets] = useState([]);

    async function fetchBudget() {
        let response = await BudgetService.fetchAll()

        setBudgets(response.data);
    }

    async function deleteBudget(id) {
        await BudgetService.delete(id);
        setBudgets(
            budgets.filter(budget => budget.id !== id)
        );
    }

    async function addBudget(budget) {
        const response = await BudgetService.add(budget);
        setBudgets([
            ...budgets,
            response.data
        ]);
    }

    const handleSearch = async (search) => {
        const data = await BudgetService.search(search);
        setBudgets(data);
    }

    async function findByDate(month) {
        const response = await BudgetService.findByDate(month);
        setBudgets(response);
    }

    useEffect(() => {

        fetchBudget();
    }, []);

    function balance() {
        let balance = 0;
        for (const element of budgets) {
            balance += Number(element.price);
        }

        return balance;
    }


    return (
        <div>

            <h1> My Budget List</h1>

            <div className="d-flex pb-3">
                <SearchBar onSearch={handleSearch} />

            </div>
            <div className="d-flex pb-3">
                <Month reset={fetchBudget} selectMonth={findByDate} />
            </div>
            <div className="d-flex pb-3">
                <h3>Balance: {balance()} €</h3>
            </div>

            <section>
                <table className="table table-info table-striped" >
                    <thead>
                        <tr>
                            <th >#</th>
                            <th>Category</th>
                            <th>Title</th>
                            <th >Price</th>
                            <th className="date" >Date</th>
                        </tr>
                    </thead>
                    <tbody>

                        {budgets.map(budget =>
                            <Budget key={budget.id}
                                budget={budget}
                                onDelete={deleteBudget}
                            />
                        )}
                    </tbody>
                </table>
            </section>

            <AddExpenses formSubmit={addBudget} />
        </div>
    )
}


