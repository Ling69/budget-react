
import {React, useState } from 'react';
import { BudgetForm } from '../components/BudgetForm';

export function AddExpenses ({formSubmit}) {

    const [open,setOpen]=useState(false);
    function handleSubmit(form){
        formSubmit(form)
        
    }

    return (
        <div>
        {open ? 
        <BudgetForm close={()=>setOpen(false)}  onFormSubmit={handleSubmit} /> : 
        <button className="btn btn-second" onClick={() => setOpen(true)}> <h3>AddExpenses</h3> </button>        
        }
        </div>
    )
}


